use svg::node::element::{Path, path::Data};
use nalgebra::{Point3, Rotation3, Vector3, Vector2};

use crate::elements::{KeplerElements, METERS_PER_AU};
 
pub fn project_point(point: &Point3<f64>) -> (f64, f64) {
    let projected_point = point / METERS_PER_AU;
    let flattened_point = projected_point.xy() * 10.0 + Vector2::new(400.0, 400.0);
    (flattened_point[0], flattened_point[1])
}

pub fn points_to_path(points: &[Point3<f64>]) -> Data {
    let mut orbit_path_data = Data::new().move_to(project_point(&points[0]));
    for point in &points[1..] {
        orbit_path_data = orbit_path_data.line_to(project_point(point));
    }
    orbit_path_data = orbit_path_data.close();

    orbit_path_data
}

pub fn orbital_elements_to_points(elements: KeplerElements, samples: usize) -> Vec<Point3<f64>> {
    // All distance variables should be in meters.
    // All angle variables should be in radians.

    let KeplerElements { e, i, o, w, .. } = elements;

    let l = elements.semilatus_rectum();
   
    let ascension_node_rotation = Rotation3::from_axis_angle(&Vector3::z_axis(), o);
    let inclination_rotation = Rotation3::from_axis_angle(&Vector3::y_axis(), i);
    let periapsis_rotation = Rotation3::from_axis_angle(&Vector3::z_axis(), w);

    let total_rotation = ascension_node_rotation * inclination_rotation * periapsis_rotation;

    (0..samples)
        .map(|sample_number| {
            let theta = (sample_number as f64) / (samples as f64) * 2.0 * std::f64::consts::PI;
            let orbital_rotation = Rotation3::from_axis_angle(&Vector3::z_axis(), theta);

            // Kepler's first law
            let distance = l / (1.0 + e * theta.cos());

            total_rotation * orbital_rotation * Point3::new(distance, 0.0, 0.0)
        })
        .collect()
}

pub fn elements_to_path(elements: KeplerElements, color: &'static str) -> Path {
    let path_points = orbital_elements_to_points(elements, 100);

    Path::new()
        .set("fill", "none")
        .set("stroke", color)
        .set("stroke-width", 2)
        .set("d", points_to_path(&path_points))
}