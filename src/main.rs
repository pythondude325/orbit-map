use nalgebra::Point3;
use svg::node::element::{path::Data, Path};

const RADIANS_PER_DEGREE: f64 = std::f64::consts::PI / 180.0;

mod path_point_drawing;
use path_point_drawing::{elements_to_path, project_point};

mod circle_transform_drawing;
use circle_transform_drawing::elements_to_circle;

pub fn create_marker(position: &Point3<f64>, color: &'static str) -> Path {
    Path::new()
        .set("fill", "none")
        .set("stroke", color)
        .set("stroke-width", 2)
        .set(
            "d",
            Data::new()
                .move_to(project_point(position))
                .move_by((-10, -10))
                .line_by((20, 20))
                .move_by((0, -20))
                .line_by((-20, 20)),
        )
}

mod elements;
use elements::{KeplerElements, METERS_PER_AU};

const HALLEY_COMET: KeplerElements = KeplerElements {
    a: 17.834 * METERS_PER_AU,
    e: 0.96714,
    i: 162.26 * RADIANS_PER_DEGREE,
    o: 58.42 * RADIANS_PER_DEGREE,
    w: 111.33 * RADIANS_PER_DEGREE,
};

const TEST_X_ORBIT: KeplerElements = KeplerElements {
    a: 20.0 * METERS_PER_AU,
    e: 0.95,
    i: 1.0 * RADIANS_PER_DEGREE,
    o: 181.0 * RADIANS_PER_DEGREE,
    w: 1.0 * RADIANS_PER_DEGREE,
};

const TEST_Y_ORBIT: KeplerElements = KeplerElements {
    a: 20.0 * METERS_PER_AU,
    e: 0.95,
    i: 1.0 * RADIANS_PER_DEGREE,
    o: 91.0 * RADIANS_PER_DEGREE,
    w: 1.0 * RADIANS_PER_DEGREE,
};

const TEST_Z_ORBIT: KeplerElements = KeplerElements {
    a: 20.0 * METERS_PER_AU,
    e: 0.95,
    i: -91.0 * RADIANS_PER_DEGREE,
    o: 1.0 * RADIANS_PER_DEGREE,
    w: 1.0 * RADIANS_PER_DEGREE,
};

fn main() {
    let day = 2458995.0;

    let document = svg::Document::new()
        .set("viewBox", (0, 0, 800, 800))
        .add(elements_to_circle(vsop87::mercury(day).into(), "gray"))
        .add(elements_to_circle(vsop87::venus(day).into(), "orange"))
        .add(elements_to_circle(vsop87::earth_moon(day).into(), "green"))
        .add(elements_to_circle(vsop87::mars(day).into(), "red"))
        .add(elements_to_circle(vsop87::jupiter(day).into(), "yellow"))
        .add(elements_to_circle(vsop87::saturn(day).into(), "orange"))
        .add(elements_to_circle(vsop87::uranus(day).into(), "cyan"))
        .add(elements_to_circle(vsop87::neptune(day).into(), "blue"))
        // .add(elements_to_circle(TEST_X_ORBIT, "red"))
        // .add(elements_to_circle(TEST_Y_ORBIT, "lime"))
        // .add(elements_to_circle(TEST_Z_ORBIT, "blue"))
        .add(elements_to_circle(HALLEY_COMET, "magenta"))
        .add(create_marker(&Point3::new(0.0, 0.0, 0.0), "red"));
    svg::save("map.svg", &document).unwrap();
}
